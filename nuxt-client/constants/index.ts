export const ErrorType = {
    Unauthorized: 401,
    Forbidden: 403,
    NotFound: 404,
    Unprocessable: 422,
  }
  
export const NavBarLink = [
    { href: '#', title: 'menu 1' },
    { href: '#', title: 'menu 2' },
    { href: '#', title: 'menu 3' },
    { href: '#', title: 'menu 4' },
  ]
  