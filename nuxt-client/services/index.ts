import { ErrorType } from '~/constants'

const createRepository = ({ app, $axios, redirect, $config }: any) => {
  $axios.onError((error: any) => {
    if (!error.response) {
      return
    }

    const code = error.response.status

    switch (code) {
      case ErrorType.Unprocessable:
        Promise.reject(error.response.data.errors)
        break
      case ErrorType.Unauthorized:
        app?.store?.dispatch('auth/logout')
        break
      case ErrorType.Forbidden:
        app?.router?.back()
        break
      case ErrorType.NotFound:
        break
      default:
        break
    }
  })

  return {
      //eximple
    //user: userRepository($config.environment, { $axios }),
  }
}

export default createRepository
