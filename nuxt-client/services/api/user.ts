import makeFactoryClass from '../class_factory'
import BaseClass from './base_class'

class User extends BaseClass {}

//Mockup data - fake du lieu
class UserMock extends User {
    login(data : any) {
        return Promise.resolve({
          token_type: 'Bearer',
          access_token:
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0Iiwicm9sZXMiOiJVU0VSUyIsIm5hbWUiOiJ0ZXN0IiwiaWF0IjoxNjQyNzM4NDM4LCJleHAiOjE2NDI3NDAyMzh9.1L-Jg7k-3l8hB8KwpQE8FddCkkyqmfL-zrPBZsT2CRPaCeB7eOMzEAvdfOBA46mcias3R9yxvTNpQs7qXyZHpg',
          expire: '2022-12-21T13:43:58.246196019',
          refresh_token:
            'gSLAIXAwJKQBgyzMQNHK0TMnuHcNDKSdj2XZ3mW8qhlmHxf9rsYLGyOiWBGhaLUe',
          refresh_expire: '2022-12-20T13:13:58.246196019',
        })
      }    
}
class UserApi extends User {
    $axios: any
    login(data: any) {
        return this.$axios.$post('/api/v1/login', data)
      }    
}

export default makeFactoryClass(UserMock, UserApi)