const makeFactoryClass =
  (MockModel: any, RealModel: any) =>
  (env : any, opts = {}) => {
    if (env === 'local') {
      return new MockModel(opts)
    }
    return new RealModel(opts)
  }

export default makeFactoryClass
