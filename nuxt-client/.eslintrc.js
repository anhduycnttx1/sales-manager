module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  extends: [
    '@nuxtjs/eslint-config-typescript',
    'plugin:nuxt/recommended',
    'prettier',
  ],
  plugins: [
    'prettier'
  ],
  // add your custom rules here
  rules: {
    "no-console": [
      2,
      {
        "allow": ["warn", "error"]
      }
    ],
    "prettier/prettier": [
      0,
      {
        "endOfLine": "auto"
      }
    ],
  },
}
