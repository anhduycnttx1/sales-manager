import useValidate from '@vuelidate/core'
import { helpers, required, email } from '@vuelidate/validators'

interface formDataLogin {
    username?: string
    password?: string
}
interface formDataRegister {
    username?: string
    password?: string
    confirm_password? : string
    email?: string
    display_name?: string
    tel?: string
}

const formRegister = (user: formDataRegister) => {
  const rules = {
    display_name: { isRequired: helpers.withMessage('', required) },
    password: { isRequired: helpers.withMessage('', required) },
    confirm_password: {
      isRequired: helpers.withMessage('', required),
      sameAs: (value: string) => value === user.password,
    },
    tel: { isRequired: helpers.withMessage('', required) },
  }
  return useValidate(rules, user)
}

const formLogin = (data: formDataLogin) => {
  const rules = {
    email: {
      isRequired: helpers.withMessage('', required),
      isEmail: helpers.withMessage('', email),
    },
    password: { isRequired: helpers.withMessage('', required) },
  }
  return useValidate(rules, data, { $scope: false })
}

const formSendEmail = (mail: string) => {
  const rules = {
    email: {
      isRequired: helpers.withMessage('', required),
      isEmail: helpers.withMessage('', email),
    },
  }
  return useValidate(rules, mail, { $scope: false })
}

const formChangeEmail = (newEmail: string) => {
  const rules = {
    email: {
      isRequired: helpers.withMessage('', required),
      isEmail: helpers.withMessage('', email),
    },
  }
  return useValidate(rules, newEmail, { $scope: false })
}

const formChangePassword = (password: string) => {
  const rules = {
    password: { isRequired: helpers.withMessage('', required) },
    confirm_password: {
      isRequired: helpers.withMessage('', required),
      sameAs: (value: string) => value === password,
    },
  }
  return useValidate(rules, password, { $scope: false })
}

export {
  formRegister,
  formLogin,
  formSendEmail,
  formChangeEmail,
  formChangePassword,
}
